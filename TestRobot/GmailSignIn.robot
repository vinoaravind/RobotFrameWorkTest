*** Settings ***
Library    SeleniumLibrary    
Resource    ObjectRepository/googleSignInPage.robot

*** Test Cases ***
Valid Login
    Given Open login page
    When Enter email ID
    And Enter password
    Then Compose email link should display

Invalid Password
    Given Open login page
    When Enter email ID
    And Enter invalid password
    Then Validate error message
       

*** Keywords ***
Open login page
    Open Browser    https://www.google.com/gmail/    chrome
    Set Browser Implicit Wait    5
Enter email ID
    Set Browser Implicit Wait    5
    Input Text    ${LoginID}    automationframework2019@gmail.com    
    Click Element    ${LoginNext}    
Enter password
    Set Browser Implicit Wait    5
    Input Password    ${LoginPassword}    Afwcwfe@25
    Click Element    ${PasswordNext}    
Compose email link should display  
    Wait Until Element Is Visible    ${ComposeEmail}    
    Log    Compose button is visibile  
    Close Browser
Enter invalid password
    Set Browser Implicit Wait    5
    Input Password    ${LoginPassword}    testapple
    Click Element    ${PasswordNext}  
Validate Error message
    ${error}    Get Text    ${ErrorMessage}
    Should Be Equal    ${ExpectedErrorMsg}    ${error}    
   
    
*** Variables ***
${error}    Test    
${ExpectedErrorMsg}    Wrong password. Try again or click Forgot password to reset it. 
       

 
        